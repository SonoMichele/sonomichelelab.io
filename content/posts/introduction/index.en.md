---
title: "Introduction"
date: 2021-12-16T16:13:45+01:00
lastmod: 2022-03-05
draft: false
toc: false
summary: ""
tags: ["general"]
weight: 100
---

Hey everyone, I'm Michele (it's like Michael but in Italian)!

"_Why make a blog?_" you may ask... Well, as I said on [my about page](/about) I am making a blog so I can write about things I create/configure (and maybe even things not IT related), keep track of them and read them if I have to do the same thing again.  
And [Kev Quirk](https://kevq.uk) made me want to write my own blog after reading [The Web Is F\*\*ked](https://thewebisfucked.com/).

This blog has multilingual support (english and italian) but since I don't want to force myself to write in both languages some posts won't get translated into english and some won't be translated into italian.

I think that's it, I don't really know what to write now so... bye :)
