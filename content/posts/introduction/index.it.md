---
title: "Introduzione"
date: 2021-12-16T16:13:45+01:00
lastmod: 2022-03-05
draft: false
toc: false
summary: ""
tags: ["general"]
weight: 100
---

Ciao a tutti, sono Michele!

"_Perché fare un blog?_" potreste chiedermi... Beh, come ho detto [nella pagina su di me](../about) sto scrivendo un blog così posso scrivere di ciò che creo/configuro (magari anche di cose non collegate all'informatica), tenerne traccia e rileggerle se devo rifare la stessa cosa.  
E [Kev Quirk](https://kevq.uk) mi ha fatto venir voglia di avere un blog dopo aver letto [The Web Is F\*\*ked](https://thewebisfucked.com/).

Questo blog ha il supporto a più lingue (inglese ed italiano) ma siccome non voglio forzarmi a scrivere in entrambe le lingue alcuni articoli non saranno tradonni in italiano e alcuni non saranno tradotti in inglese.

Penso che per ora sia tutto, Non so che altro scrivere, quindi... ciao :)
