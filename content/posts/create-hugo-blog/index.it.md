---
draft: false
title: "Creare un blog con Hugo"
date: 2022-01-15T11:48:03+01:00
lastmod: 2022-03-05
showtoc: true
tags: ["tutorial"]
description: "In questo articolo parlo di come creare un semplice blog con Hugo e il tema hello-friend-ng"
summary: "In questo articolo parlo di come creare un semplice blog con Hugo e il tema hello-friend-ng"
---

# Introduzione

## Cos'è Hugo?

[Hugo](https://gohugo.io) è un [generatore di siti statici](https://www.netlify.com/blog/2020/04/14/what-is-a-static-site-generator-and-3-ways-to-find-the-best-one/), quindi un programma che prende dei contenuti e dei template, li unisce e genera dei file HTML pronti ad essere inviati all'utente che li richiede.  
Questa caratteristica ci evita di dover scrivere file HTML a mano per ogni articolo/pagina che vogliamo pubblicare evitandoci allo stesso tempo il dover gestire un server e un database.  
Costruendo il nostro sito prima di pubblicarlo ne rendiamo il caricamento più veloce perché non viene ricostruito per ogni richiesta fatta dagli utenti, questo ci porta anche ad avere un sito più facilmente scalabile e meno costoso, infatti è possibile avere un blog a costo 0 e senza dover gestire un proprio server aumentando al tempo stesso la sicurezza del blog o sito.

## Installazione

Hugo viene distribuito come un singolo file eseguibile scaricabile dalla pagina ufficiale su [GitHub](https://github.com/gohugoio/hugo/releases).  
Oltre all'installazione utilizzando l'eseguibile è possibile utilizzare i seguenti metodi in base al sistema operativo che stai usando (ne puoi trovare altri nella [pagina ufficiale](https://gohugo.io/getting-started/installing) di Hugo):

> Linux  
> `$ snap install hugo`

> MacOS  
> `$ brew install hugo`

> Windows  
> `$ choco install hugo -confirm`

# Creazione blog

I comandi in questo articolo vengono eseguiti su una [shell](<https://it.wikipedia.org/wiki/Shell_(informatica)>) compatibile con lo standard [POSIX](https://it.wikipedia.org/wiki/POSIX)
Dopo aver [installato](#installazione) Hugo possiamo utilizzare il comando

```bash
hugo new site /percorso/nome_sito
```

per creare la struttura base del nostro sito che, alla versione 0.91.2 di Hugo, equivale a questa

```
nome_sito
├── archetypes/
│  └── default.md
├── config.toml
├── content/
├── data/
├── layouts/
├── static/
└── themes/
```

il prossimo passo se non vuoi creare il tuo tema è quello di installarne uno dei tanti che puoi trovare sul [sito ufficiale dei temi](https://themes.gohugo.io) (sono più di 300).

## Installare un tema

In questo tutorial installeremo il tema [hello-friend-ng](https://github.com/rhazdon/hugo-theme-hello-friend-ng/).  
Per scaricare un tema esistono vari metodi ma io te ne elencherò due.

### Scaricare il file zip del tema

Scarica il [file zip](https://github.com/rhazdon/hugo-theme-hello-friend-ng/archive/refs/heads/master.zip), aprilo ed estrai il contenuto nella cartella `themes` in modo da ottenere questa struttura

```
hello-friend-ng
├── archetypes
├── assets
├── CHANGELOG.md
├── CONTRIBUTING.md
├── data
├── docs
├── exampleSite
├── i18n
├── images
├── layouts
├── LICENSE.md
├── README.md
├── static
└── theme.toml

```

### Usare i sottomoduli di Git

> Requisiti  
> [git](https://git-scm.com/)

Per aggiungere il tema come sottomodulo di git dobbiamo prima inizializzare un nostro repository git all'interno della cartella `/percorso/nome_sito` che abbiamo creato [in precedenza](#creazione-blog)

```bash
git init
```

Ora possiamo aggiungere il nostro tema con questo comando

```bash
git submodule add https://github.com/rhazdon/hugo-theme-hello-friend-ng.git themes/hello-friend-ng
```

## Configurare il blog

Una volta installato il tema possiamo iniziare a configurare il nostro blog modificando il file `config.toml`
`yaml`, `toml`, `json`

```toml
baseurl      = "https://esempio.it/"
title        = "Il mio blog"
languageCode = "it"
theme        = "hello-friend-ng"
paginate     = 10

[params]
  dateform        = "Jan 2, 2006"
  dateformShort   = "Jan 2"
  dateformNum     = "2006-01-02"
  dateformNumTime = "2006-01-02 15:04"

  # Sottotitolo per la pagina principale
  homeSubtitle = "Un bellissimo sottotitolo"

  [params.logo]
    logoText = "SonoMichele"

[taxonomies]
    category = "blog"
    tag      = "tags"
    series   = "series"

# Creiamo un menu di navigazione
[[menu.main]]
  identifier = "blog"
  name       = "Blog"
  url        = "/posts"
```

Puoi trovare la lista di tutti i parametri configurabili di Hugo nella sua [documentazione](https://gohugo.io/getting-started/configuration/#all-configuration-settings).  
Vediamo comunque un po' più nel dettaglio la configurazione che abbiamo creato poco fa:

- **baseUrl**: È l'indirizzo a cui sarà disponibile il sito una volta pubblicato, nel mio caso è https://www.micheleviotto.it/
- **title**: È il titolo della pagina principale e generalmente viene utilizzato anche dai temi all'interno delle pagine
- **languageCode**: È il codice della lingua del nostro sito secondo lo standard [RFC5646](https://datatracker.ietf.org/doc/html/rfc5646)
- **theme**: È il nome del tema che vogliamo utilizzare e deve corrispondere al nome della cartella all'interno di `themes`
- **params**: In questa sezione vengono inseriti parametri di [configurazione del tema](https://github.com/rhazdon/hugo-theme-hello-friend-ng/blob/848b6879b3a2a201510bd11dadf37826ee038616/exampleSite/config.toml#L50), quindi questi parametri possono variare da tema a tema
  - **homeSubtitle**: È il sottotitolo che viene mostrato nella pagina principale del sito
  - **logoText**: È il testo che mostrato in alto a sinistra nella navbar

## Creare un articolo

Hugo si aspetta che i contenuti del nostro sito siano all'interno della cartella `contents`. Generalmente gli articoli per un blog vengono creati all'interno della cartella `contents/posts` e saranno accessibili all'url `baseUrl/posts/nome_articolo`.

Per creare un articolo Hugo ci mette a disposizione il comando `hugo new`, che è lo stesso che abbiamo utilizzato per generare la struttura del nostro sito ma se invece di `site` come parametro gli passiamo un percorso con un nome di file lo creerà all'interno della cartella `contents`.

Alla creazione dell'articolo utilizzando il seguente comando Hugo ci aggiungerà di base il [front matter](#front-matter) che si trova all'interno del file `archetypes/posts.md` del tema che stiamo utilizzando

```bash
hugo new posts/primo_articolo.md
```

risulterà quindi nella creazione di `contents/posts/primo_articolo.md` che contiene

```yaml
---
title: "Primo_articolo"
date: 2022-01-14T22:55:25+01:00
draft: true
toc: false
images:
tags:
  - untagged
---

```

Di seguito un esempio di un articolo scritto in markdown

```markdown
---
title: "Primo_articolo"
date: 2022-01-14T22:55:25+01:00
draft: true
toc: true
images:
tags:
  - untagged
---

## Un titolo dell'argomento

Qui posso scrivere del testo e posso anche utilizzare il **grassetto** o _il corsivo_.

### Posso anche fare delle liste

- Banana
- Gatto

### anche numerate

1. Giovanni
2. Francesco

## un altro titolo per la tabella dei contenuti
```

{{< figure src="images/post_preview_it.png" caption="<center><small><i>Articolo visualizzato nel browser</i></small></center>" >}}

### Front matter

Il front matter ci permette di allegare dei metadati al nostro articolo come ad esempio:

- **title**: il titolo dell'articolo
- **date**: di base Hugo la popola con la data in cui è stato creato il file
- **draft**: ci permette di dire ad Hugo se questo articolo è una bozza, se è impostato a `true` di base Hugo non mostrerà questo articolo nel nostro sito

## Avviare Hugo per lo sviluppo

Per vedere l'articolo che abbiamo appena creato possiamo avviare hugo in modalità sviluppo e fargli servire il nostro sito in locale con il comando

```bash
hugo serve -D
```

la flag `-D` dice ad Hugo di mostare anche gli articoli in cui `draft` è impostato a `true`.

Possiamo accedere e visualizzare il nostro blog all'indirizzo http://localhost:1313

# Conclusioni

Spero di esserti stato d'aiuto nella creazione del tuo blog con Hugo, sto anche pianificando un altro articolo in cui spiego come pubblicare il nostro blog su internet con [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

Ti consiglio di dare un'occhiata anche alla [documentazione ufficiale di Hugo](https://gohugo.io/documentation/) perché io ho saltato molte cose che Hugo o i temi possono offrirti.
