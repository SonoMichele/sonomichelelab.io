---
draft: false
title: "Contattami"
date: 2022-04-21
lastmod: 2022-04-21
description: "Qui puoi trovare i miei vari account che puoi usare per contattarmi"
toc: false
disableShare: true
---

## Email: [michele@viotto.me](mailto:michele@viotto.me)

## Mastodon: [@SonoMichele](https://fosstodon.org/@SonoMichele)

## Twitter: [@SonoMicheleYa](https://twitter.com/SonoMicheleYa)
