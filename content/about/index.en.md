---
draft: false
title: "About Me"
date: 2021-12-16T10:48:53+01:00
lastmod: 2022-01-02
description: "Some things about who I am and some of my interests"
toc: false
disableShare: true
---

## Hey I'm Michele. A Backend Developer and in general an IT enthusiast.

Welcome to my little space on the internet.

I started caring about my privacy on the internet about a year ago and I deleted all my accounts from Meta (formerly know as Facebook) owned services. The only social I sometimes use is [Mastodon](https://joinmastodon.org).

I started this blog to have a place where to publish my thoughts and updates on what I'm doing and also as a kind of portfolio.

Probably no one will ever read this blog but it will be useful for my future self when I'll have to redo something I already did and wrote about.

Have a nice day!

---

## Current interests

- [GoLang](https://go.dev) (For backend software)
- [Deep Rock Galactic](https://www.deeprockgalactic.com) (For having fun in free time)
- [The Office](https://en.wikipedia.org/wiki/The_Office) (I actually just finished it but I loved it)

---

## Work experiences

1. **Centro Sistemi Treviso** (20/05/2019-14/06/2019, Italy)  
   Custom pages for employees healtcare on internal ERP software
2. **Fox Information Technology** (01/07/2019-26/07/2019, United Kingdom)  
   Laptop configuration for a school and security software updates on various software
3. **Aton S.p.A.** (07/06/2021-30/07/2021, Italy)  
   Terraform infrastructure for blue/green deployment and other pipeline automations

---

## Personal projects

- _[Synthetic Stars](https://syntheticstars.org)_
  - _[Single Sign-On](https://gitlab.com/synthetic-stars/sso-backend)_: A custom Single Sign-On system developed in TypeScript and [NestJS](https://nestjs.com)
  - _[Gameserver Manager](https://gitlab.com/synthetic-stars/gameserver-manager)_: A little program in Go to start and stop dockerized servers for the matches
- _[Old portfolio](https://gitlab.com/SonoMichele/personal-portfolio)_: My old website, made with Flask and [Tailwind css](https://tailwindcss.com)
- _[BetterPM](https://gitlab.com/SonoMichele/betterpm-minetest)_: A [Minetest](https://www.minetest.net) mod developed in Lua that adds private messages
