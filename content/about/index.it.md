---
draft: false
title: "Su di Me"
date: 2021-12-16T10:48:53+01:00
lastmod: 2022-01-02
description: "Alcune cose su di me e sui miei interessi"
toc: false
disableShare: true
---

## Hey sono Michele. Sviluppatore Backend e in generale appassionato di informatica.

Benvenuto nel mio piccolo spazio su internet.

Ho iniziato a preoccuparmi per la mia privacy su internet circa un anno fa e ho cancellato i miei account su tutti i servizi di Meta (conosciuta in passato come Facebook). L'unico social che uso qualche volta è [Mastodon](https://joinmastodon.org).

Ho inziato questo blog per avere un post dove pubblicare i miei pensieri, aggiornamenti su quello che sto facendo e anche come una specie di portfolio.

Probabilmente nessuno leggerà mai questo blog ma sarà utile al futuro me quando dovrò rifare qualcosa di cui ho già scritto.

Buona giornata!

---

## Interessi attuali

- [GoLang](https://go.dev) (Per applicativi backend)
- [Deep Rock Galactic](https://www.deeprockgalactic.com) (Per divertirmi nel tempo libero)
- [The Office](https://en.wikipedia.org/wiki/The_Office) (L'ho finita poco fa ma l'ho adorata)

---

## Esperienze lavorative

1. **Centro Sistemi Treviso** (20/05/2019-14/06/2019, Italia)  
   Pagine personalizzate per la gestione della salute dei lavorati sull'applicativo ERP interno
2. **Fox Information Technology** (01/07/2019-26/07/2019, Regno Unito)  
   Configurazione portatili per una scuola e aggiornamenti di sicurezza su vari applicativi
3. **Aton S.p.A.** (07/06/2021-30/07/2021, Italia)  
   Infrastruttura Terraform per la gestione del blue/green deployment e pipeline per automazione

---

## Progetti personali

- _[Synthetic Stars](https://syntheticstars.org)_
  - _[Single Sign-On](https://gitlab.com/synthetic-stars/sso-backend)_: Un sistema di Single Sign-On sviluppato in TypeScript e [NestJS](https://nestjs.com)
  - _[Gameserver Manager](https://gitlab.com/synthetic-stars/gameserver-manager)_: Un piccolo programma scritto in Go per avviare e fermare i server dockerizzati per le partite
- _[Old portfolio](https://gitlab.com/SonoMichele/personal-portfolio)_: Il mio vecchio sito, sviluppato con Flask e [Tailwind css](https://tailwindcss.com)
- _[BetterPM](https://gitlab.com/SonoMichele/betterpm-minetest)_: Una mod per [Minetest](https://www.minetest.net) sviluppata in Lua che aggiunge i messaggi privati
